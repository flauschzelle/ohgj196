require "lib.slam"
vector = require "lib.hump.vector"
tlfres = require "lib.tlfres"

require "helpers"

CANVAS_WIDTH = 1920
CANVAS_HEIGHT = 1080

GRID_WIDTH = 17
GRID_HEIGHT = 9
TILE_SIZE = 100

player = {x=5,y=5}

original_duration = 0.5
turn_duration = original_duration
remaining = turn_duration

loot = {{x=4,y=4},
        {x=3,y=7},
        {x=1,y=5},
        {x=7,y=4},
        {x=2,y=3}}

score = 0
state = "title"

f = 100/16

function love.load()
    -- set up default drawing options
    love.graphics.setDefaultFilter( "nearest", "nearest", 1 )
    love.graphics.setBackgroundColor(0, 0, 0)

    -- load assets
    images = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("images")) do
        if filename ~= ".gitkeep" then
            images[filename:sub(1,-5)] = love.graphics.newImage("images/"..filename)
        end
    end

    sounds = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("sounds")) do
        if filename ~= ".gitkeep" then
            sounds[filename:sub(1,-5)] = love.audio.newSource("sounds/"..filename, "static")
        end
    end
    sounds.pickup:setVolume(0.1)

    music = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("music")) do
        if filename ~= ".gitkeep" then
            music[filename:sub(1,-5)] = love.audio.newSource("music/"..filename, "stream")
            music[filename:sub(1,-5)]:setLooping(true)
        end
    end

    fonts = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("fonts")) do
        if filename ~= ".gitkeep" then
            fonts[filename:sub(1,-5)] = {}
            for fontsize=50,100 do
                fonts[filename:sub(1,-5)][fontsize] = love.graphics.newFont("fonts/"..filename, fontsize)
            end
        end
    end
    love.graphics.setFont(fonts["m5x7"][100])

    grid = {}
    for x=1,GRID_WIDTH do
        table.insert(grid, {})
        for y=1,GRID_HEIGHT do
            v = false
            --if love.math.random() < 0.9 then
                v = true
            --end
            table.insert(grid[x], v)
        end
    end

    respawn()
end

function love.update(dt)
    if state == "game" then
        remaining = remaining - dt

        if remaining <= 0 then
            die()
        end
    end
end

function count()
    c = 0
    for x=1,GRID_WIDTH do
        for y=1,GRID_HEIGHT do
            if tile(x, y) then
                c = c+1
            end
        end
    end
    return c
end

function die()
    grid[player.x][player.y] = false
    love.audio.play(sounds.die)

    if count() == 0 then
        state = "gameover"
        return
    end
    respawn()
    turn_duration = original_duration
end

function respawn()
    ok = false
    while not ok do
        x = love.math.random(1, GRID_WIDTH)
        y = love.math.random(1, GRID_HEIGHT)
        if tile(x, y) then
            ok = true
        end
    end

    player.x = x
    player.y = y
    remaining = turn_duration
end

function love.mouse.getPosition()
    return tlfres.getMousePosition(CANVAS_WIDTH, CANVAS_HEIGHT)
end

function tile(x, y)
    return grid[x] and grid[x][y]
end

function love.keypressed(key)
    renew = false

    if state == "title" then
        state = "game"
    end

    if key == "escape" then
        love.window.setFullscreen(false)
        love.event.quit()
    elseif key == "up" then
        if tile(player.x, player.y-1) then
            player.y = player.y-1
            renew = true
        end
    elseif key == "down" then
        if tile(player.x, player.y+1) then
            player.y = player.y+1
            renew = true
        end
    elseif key == "left" then
        if tile(player.x-1, player.y) then
            player.x = player.x-1
            renew = true
        end
    elseif key == "right" then
        if tile(player.x+1, player.y) then
            player.x = player.x+1
            renew = true
        end
    --elseif key == "f" then
    --    isFullscreen = love.window.getFullscreen()
    --    love.window.setFullscreen(not isFullscreen)
    end

    if player.x > GRID_WIDTH then
        player.x = GRID_WIDTH
    end
    if player.x < 1 then
        player.x = 1
    end
    if player.y > GRID_HEIGHT then
        player.y = GRID_HEIGHT
    end
    if player.y < 1 then
        player.y = 1
    end

    if renew then
        turn_duration = 0.99*turn_duration
        remaining = turn_duration
    end
    for l=1,5 do
        if (player.x == loot[l].x and player.y == loot[l].y) then
            pickupLoot(l)
        end
    end
end

function pickupLoot(lootnr)
    --play sound
    sounds.pickup:setPitch(math.random(90, 110)/100)
    love.audio.play(sounds.pickup)
    --increase score
    score = score + 1
    --new loot position:
    ok = false
    while not ok do
        x = love.math.random(1, GRID_WIDTH)
        y = love.math.random(1, GRID_HEIGHT)
        if tile(x, y) then
            ok = true
        end
    end
    loot[lootnr].x = x
    loot[lootnr].y = y
end

function love.keyreleased(key)
end

function love.mousepressed(x, y, button)
    --sounds.meow:setPitch(0.5+math.random())
    --sounds.meow:play()
end

function love.draw()
    love.graphics.setColor(255, 255, 255, 255)
    tlfres.beginRendering(CANVAS_WIDTH, CANVAS_HEIGHT)

    -- Draw the game here!

    if state == "gameover" then
        love.graphics.print("Game over", 400, 400)
    elseif state == "title" then
        love.graphics.draw(images.title, 0, 0, 0, 10, 10)
        love.graphics.print("Made by @blinry and @flauschzelle", 390, 600)
        love.graphics.print("for the 196th One Hour Game Jam", 390, 670)
        love.graphics.print("Help the little lizard to collect", 390, 670+70+80)
        love.graphics.print("all the gems! :)", 390, 670+2*70+80)
        love.graphics.print("But remember to keep moving!", 390, 670+3*70+2+80)
    else
        for x=1,GRID_WIDTH do
            for y=1,GRID_HEIGHT do
                if grid[x][y] then
                    love.graphics.setColor(0.5, 0.5, 0.5)
                    love.graphics.draw(images.stone, x*TILE_SIZE, y*TILE_SIZE, 0, f, f)
                else
                    love.graphics.setColor(1, 1, 1)
                    love.graphics.draw(images.lava, x*TILE_SIZE, y*TILE_SIZE, 0, f, f)
                end
            end
        end
    
        --loot
        love.graphics.setColor(1, 1, 1)
        for l=1,5 do
            love.graphics.draw(images.loot, loot[l].x*TILE_SIZE, loot[l].y*TILE_SIZE, 0, f, f)
        end
        --player
        love.graphics.setColor(1, 1, 1)
        love.graphics.draw(images.player, player.x*TILE_SIZE, player.y*TILE_SIZE, 0, f, f)


        --load bar
        love.graphics.setColor(0.2, 0.2, 0.2)
        h = 50
        love.graphics.rectangle("fill", 0, 1080-h, 1920, h)
        rw = remaining/turn_duration*1920
        love.graphics.setColor(0.8, 0.5, 0.1)
        love.graphics.rectangle("fill", 0, 1080-h, rw, h)

        love.graphics.setColor(1, 1, 1)
        love.graphics.print("Gems: "..score, 10, 10)
    end

    tlfres.endRendering()
end
